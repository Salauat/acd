package kz.aitu.week2;
import java.util.Scanner;

public class Practice1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x= input.nextInt();

        System.out.println(fib(x));
    }

    public static int fib(int x){
        if(x == 0 || x == 1) return x;
        return fib(x-1) + fib(x-2);
    }
}

