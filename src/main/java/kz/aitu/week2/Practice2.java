package kz.aitu.week2;
import java.util.Scanner;
public class Practice2 {

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int x;
        x = input.nextInt();

        System.out.println(reverseFunction(x));
    }

    public static int reverseFunction(int x){
        Scanner newScanner = new Scanner(System.in);
        int y = newScanner.nextInt();

        if(x == 1) return y;

        System.out.println(reverseFunction(x-1));
        return y;
    }
}
