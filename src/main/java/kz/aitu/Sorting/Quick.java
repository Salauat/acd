package kz.aitu.Sorting;

public class Quick {
    public static void sort(int[] arr) {
        if (arr == null || arr.length == 0)
            return;
        int left = 0;
        int right = 0;
        if (left >= right)
            return;
        int center = left + (right - left) / 2;
        int base = arr[center];
        int i = left, j = right;
        while (i <= j) {
            while (arr[i] < base) {
                i++;
            }

            while (arr[j] > base) {
                j--;
            }

            if (i <= j) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }
        if (left < j)
            sort(arr);

        if (right > i)
            sort(arr);
    }
    }
