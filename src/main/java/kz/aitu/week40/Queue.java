package kz.aitu.week40;

public class Queue {
    private Node head;
    private Node tail;

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public Node getTail() {
        return tail;
    }

    public void push(int value){
        Node newNode = new Node(value);
        if (head==null){
            head = newNode;
            tail = newNode;
        } else {
            tail.setNext(newNode);
            tail = newNode;
        }
    }

    public void pop(){
        System.out.println(head.getValue());
        head = head.getNext();
    }

    public boolean empty(){
        if (head==null){
            return true;
        }
        return false;
    }

    public int size(){
        int x = 0;
        while (head!=null){
            head = head.getNext();
            x++;
        }
        return x;
    }
}

