package kz.aitu.week40;

public class Main {
    public static int print(Queue queue){
        Node node = queue.getHead();
        int c = 0;
        while (node!=null){
            System.out.print(node.getValue()+" ");
            node = node.getNext();
            c++;
        }
        System.out.println();
        return c;
    }

    public static void main(String[] args) {
        Queue newQueue = new Queue();
        newQueue.push(1);
        newQueue.push(11);
        System.out.println(print(newQueue));
        newQueue.pop();
        System.out.println(newQueue.empty());
    }
}
