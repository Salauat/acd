package kz.aitu.week40;

public class Node {
    private int value;
    private kz.aitu.week40.Node next;

    public void setNext(kz.aitu.week40.Node next) {
        this.next = next;
    }

    public kz.aitu.week40.Node getNext() {
        return next;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node(int value){
        this.value = value;
    }
}
