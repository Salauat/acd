package kz.aitu.Midterm;

public class Practice4 {

    public static int Buy(int arr[], int x) {
        int toBuy = arr[0];

        for (int i = 1; i < x; i++)
            toBuy = Math.min(toBuy, arr[i]);
        return toBuy;
    }

    public static int Sell(int arr[], int x) {
        int sell = arr[0];

        for (int i = 1; i < x; i++)
            sell = Math.max(sell, arr[i]);
        return sell;
    }

    public static void main(String[] args) {

        int arr[] = { 10, 7, 5, 8, 11 };
        int x = arr.length;
        System.out.println( "Buy - "+ Buy(arr, x));
        System.out.println( "Sell - " + Sell(arr, x));
    }

}

