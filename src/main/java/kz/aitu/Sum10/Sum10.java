package kz.aitu.Sum10;

import java.util.Arrays;

import java.util.Scanner;

public class Sum10 {
    public static void main(String[] args) {
        int[] arrayofIntegers = {0, 2, 4, 6, 8, 10};
        Arrays.sort(arrayofIntegers);
        System.out.println(Arrays.toString(arrayofIntegers));
        int left = 0;
        int right = arrayofIntegers.length - 1;
        while (left < right) {
            int leftval = arrayofIntegers[left];
            int rightval = arrayofIntegers[right];
            int sum = leftval + rightval;
            if (sum == 10) {
                System.out.println(arrayofIntegers[left] + "," + arrayofIntegers[right]);
                right--;
                left++;
            }
            if (sum > 10) {
                right--;
            }
            if (sum < 10) {
                left++;
            }
        }
    }
}
